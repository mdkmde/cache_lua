t = require("test")
c = require("cache").init(3)

-- set/get
t.run("c.set(1,1)", c.set, {1, 1}, {})
t.run("c.get(1)", c.get, {1}, {1})
t.run("c.set(2,2)", c.set, {2, 2}, {})
t.run("c.get(2)", c.get, {2}, {2})
t.run("c.set(3,3)", c.set, {3, 3}, {})
t.run("c.get(3)", c.get, {3}, {3})
t.run("c.get(1)", c.get, {1}, {1})
-- last (2) will be dropped
t.run("c.set(4,4)", c.set, {4, 4}, {})
t.run("c.get(4)", c.get, {4}, {4})
t.run("c.get(1)", c.get, {1}, {1})
t.run("c.get(2)", c.get, {2}, {})
t.run("c.getTTL(1)", c.getTTL, {1}, {-1})
t.run("c.getTTL(2)", c.getTTL, {2}, {})

-- check ttl
t.run("c.set(1)", c.set, {1, 1, 1}, {})
t.run("c.getTTL(1) - might fail", c.getTTL, {1}, {1})
t.run("c.get(1)", c.get, {1}, {1})
io.write("sleep 2s to invalidate ttl\n")
io.flush()
os.execute("sleep 2")
t.run("c.getTTL(1)", c.getTTL, {1}, {0})
t.run("c.get(1)", c.get, {1}, {})
t.run("c.getTTL(1)", c.getTTL, {1}, {})

t.summary()

