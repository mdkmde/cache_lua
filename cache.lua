--[[
https://gitlab.com/mdkmde/cache_lua/ - matthias (at) koerpermagie.de

Copyright (c) ISC License - Matthias Diener

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
]]

local date = os.date
local match = string.match
_ENV = nil

local getTx = function()
  local n_year, n_day, n_hour, n_minute, n_second = match(date("!%Y%j%H%M%S"), "(%d%d%d%d)(%d%d%d)(%d%d)(%d%d)(%d%d)")
  -- if ttl is more than a year it won't work; year is just prefix, will reset cache on Jan 01 00:00:00
  return (n_year * 100000000) + ((((((n_day * 24) + n_hour) * 60) + n_minute) * 60) + n_second)
end

local KEY = 1
local VAL = 2
local TTL = 3
local PRV = 4
local NXT = 5

local init = function(in_max_size)
-- returns table with get and set function
  local n_size = in_max_size
  local t_data = {}
  local t_first = nil
  local t_last = nil

  local popElement = function(it_value)
    local t_prev = it_value[PRV]
    if t_prev then
      t_prev[NXT] = it_value[NXT]
    else
      t_first = it_value[NXT]
    end
    if it_value[NXT] then
      it_value[NXT][PRV] = it_value[PRV]
    else
      t_last = it_value[PRV]
    end
    it_value[PRV] = nil
    it_value[NXT] = nil
  end

  local pushFirst = function(it_value)
    if t_first then
      it_value[NXT] = t_first
      t_first[PRV] = it_value
    end
    t_first = it_value
    if not(t_last) then
      t_last = it_value
    end
  end

  local set = function(is_key, i_value, in_ttl)
    if t_data[is_key] then
      popElement(t_data[is_key])
      n_size = n_size + 1
      t_data[is_key] = nil
    end
    if i_value == nil then
      return
    end
    t_data[is_key] = {is_key, i_value, in_ttl and getTx() + in_ttl}
    pushFirst(t_data[is_key])
    if n_size == 0 then
      t_data[t_last[KEY]] = nil
      t_last = t_last[PRV]
      t_last[NXT][PRV] = nil
      t_last[NXT] = nil
    else
      n_size = n_size - 1
    end
  end

  local get = function(is_key)
    local t_value = t_data[is_key]
    if t_value then
      if t_value[TTL] then
        if t_value[TTL] < getTx() then
          set(is_key)
          return
        end
      end
      popElement(t_value)
      pushFirst(t_value)
      return t_value[VAL]
    end
  end

  local getTTL = function(is_key)
    local t_value = t_data[is_key]
    if t_value then
      if t_value[TTL] then
        local n_value = t_value[TTL] - getTx()
        return 0 < n_value and n_value or 0
      else
        return -1
      end
    else
      return
    end
  end
  return {get = get, set = set, getTTL = getTTL}
end

return {
  init = init, --[[ (max size of cache elements)
    returns table with get and set function {
      get (key)
      set (key, value, optional ttl)
    } ]]
}

